import hashlib
import json
import os
import platform
import shutil
import subprocess
import sys
import time
import zipfile
from pathlib import Path

import requests

from config import MC_CLIENTS_DIR, WEB_SERVER_HOST, AUTH_SERVER_HOST, DOWNLOAD_HOST

DIRS_TO_UPDATE = [
    "mods",
    "asm",
    "config",
    "libraries",
    "resourcepacks",
    "versions",
]


def get_natives_string(lib):
    if platform.architecture()[0] == "64bit":
        arch = "64"
    elif platform.architecture()[0] == "32bit":
        arch = "32"
    else:
        raise Exception("Architecture not supported")

    natives_file = ""
    if "natives" not in lib:
        return natives_file

    if "windows" in lib["natives"] and platform.system() == 'Windows':
        natives_file = lib["natives"]["windows"].replace("${arch}", arch)
    elif "osx" in lib["natives"] and platform.system() == 'Darwin':
        natives_file = lib["natives"]["osx"].replace("${arch}", arch)
    elif "linux" in lib["natives"] and platform.system() == "Linux":
        natives_file = lib["natives"]["linux"].replace("${arch}", arch)
    else:
        raise Exception("Platform not supported")

    return natives_file


"""
[Parses "rule" subpropery of library object, testing to see if should be included]
"""


def should_use_library(lib):
    def rule_says_yes(rule):
        use_lib = None

        if rule["action"] == "allow":
            use_lib = False
        elif rule["action"] == "disallow":
            use_lib = True

        if "os" in rule:
            for key, value in rule["os"].items():
                cur_os = platform.system()
                if key == "name":
                    if value == "windows" and cur_os != 'Windows':
                        return use_lib
                    elif value == "osx" and cur_os != 'Darwin':
                        return use_lib
                    elif value == "linux" and cur_os != 'Linux':
                        return use_lib
                elif key == "arch":
                    if value == "x86" and platform.architecture()[0] != "32bit":
                        return use_lib

        return not use_lib

    if "rules" not in lib:
        return True

    is_should_use_library = False
    for i in lib["rules"]:
        if rule_says_yes(i):
            return True

    return is_should_use_library


"""
[Get string of all libraries to add to java classpath]
"""


def get_classpath(lib, cur_mc_dir):
    cp = []

    for i in lib["libraries"]:
        if not should_use_library(i):
            continue

        lib_domain, lib_name, lib_version = i["name"].split(":")
        jar_path = os.path.join(cur_mc_dir, "libraries", *lib_domain.split('.'), lib_name, lib_version)

        native = get_natives_string(i)
        jar_file = lib_name + "-" + lib_version + ".jar"
        if native != "":
            jar_file = lib_name + "-" + lib_version + "-" + native + ".jar"

        cp.append(os.path.join(jar_path, jar_file))

    cp.append(os.path.join(cur_mc_dir, "versions", lib["id"], f'{lib["id"]}.jar'))

    return os.pathsep.join(cp)


def get_available_versions(session: requests.Session):
    response = session.get(f"{WEB_SERVER_HOST}/launcher/clients")
    client_names = []
    for item in response.json():
        client_names.append(item["name"])
    return client_names


def download_client(download_label, session: requests.Session, client_name):
    try:
        client_download_link_response = session.get(f"{WEB_SERVER_HOST}/launcher/"
                                                    f"get-client-download-link?"
                                                    f"client_name={client_name}")
    except:
        return "Can't connect to server"
    response = requests.get(f'{DOWNLOAD_HOST}{client_download_link_response.json()["data"]["link"]}', stream=True)
    print(response)
    installing_zip_path = os.path.join(MC_CLIENTS_DIR, "installing.zip")
    total_length = response.headers.get('content-length')
    with open(installing_zip_path, 'wb') as f:
        if total_length is None:  # no content length header
            f.write(response.content)
        else:
            start = time.perf_counter()
            dl = 0
            total_length = int(total_length)
            total_length_mb = round(total_length / 1024 / 1024, 2)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                dl_mb = round(dl / 1024 / 1024, 2)
                f.write(data)
                done_percents = int(100 * dl / total_length)
                cur_speed_mps = round((dl//(time.perf_counter() - start)) / 1024 / 1024, 2)
                time_left = int(round((total_length_mb - dl_mb) / cur_speed_mps / 60, 0))
                download_label["text"] = f"Загружено {dl_mb}mb / {total_length_mb}mb ({done_percents}%)\n" \
                                         f"Скорость {cur_speed_mps}mb/s\nОсталось {time_left} минут"
                download_label.update()

    with zipfile.ZipFile(installing_zip_path, 'r') as zip_ref:
        zip_ref.extractall(MC_CLIENTS_DIR)

    with open(os.path.join(MC_CLIENTS_DIR, "installing.zip"), 'rb') as f:
        current_version_hash = hashlib.sha256(f.read()).hexdigest()

    with open(os.path.join(MC_CLIENTS_DIR, f".{client_name}", ".ver"), "w") as f:
        f.write(current_version_hash)

    os.remove(installing_zip_path)


def update_client(download_label, session: requests.Session, client_name):
    try:
        client_download_link_response = session.get(f"{WEB_SERVER_HOST}/launcher/"
                                                    f"get-client-download-link?"
                                                    f"client_name={client_name}")
    except Exception as e:
        return "Can't connect to server"

    os.rename(os.path.join(MC_CLIENTS_DIR, f".{client_name}"), os.path.join(MC_CLIENTS_DIR, f".{client_name}_old"))

    for item in DIRS_TO_UPDATE:
        if os.path.exists(os.path.join(MC_CLIENTS_DIR, f".{client_name}_old", item)):
            shutil.rmtree(os.path.join(MC_CLIENTS_DIR, f".{client_name}_old", item))

    response = session.get(f'{DOWNLOAD_HOST}{client_download_link_response.json()["data"]["link"]}', stream=True)
    updating_zip_path = os.path.join(MC_CLIENTS_DIR, "updating.zip")
    total_length = response.headers.get('content-length')
    with open(updating_zip_path, 'wb') as f:
        if total_length is None:  # no content length header
            f.write(response.content)
        else:
            start = time.perf_counter()
            dl = 0
            total_length = int(total_length)
            total_length_mb = round(total_length / 1024 / 1024, 2)
            for data in response.iter_content(chunk_size=4096):
                dl += len(data)
                dl_mb = round(dl / 1024 / 1024, 2)
                f.write(data)
                done_percents = int(100 * dl / total_length)
                cur_speed_mps = round((dl // (time.perf_counter() - start)) / 1024 / 1024, 2)
                time_left = int(round((total_length_mb - dl_mb) / cur_speed_mps / 60, 0))
                download_label["text"] = f"Загружено {dl_mb}mb / {total_length_mb}mb ({done_percents}%)\n" \
                                         f"Скорость {cur_speed_mps}mb/s\nОсталось {time_left} минут"
                download_label.update()

    with zipfile.ZipFile(updating_zip_path, 'r') as zip_ref:
        zip_ref.extractall(MC_CLIENTS_DIR)

    with open(os.path.join(MC_CLIENTS_DIR, "updating.zip"), 'rb') as f:
        current_version_hash = hashlib.sha256(f.read()).hexdigest()

    with open(os.path.join(MC_CLIENTS_DIR, f".{client_name}_old", ".ver"), "w") as f:
        f.write(current_version_hash)
    os.remove(os.path.join(MC_CLIENTS_DIR, "updating.zip"))

    for item in DIRS_TO_UPDATE:
        if os.path.exists(os.path.join(MC_CLIENTS_DIR, f".{client_name}", item)):
            os.rename(os.path.join(MC_CLIENTS_DIR, f".{client_name}", item),
                      os.path.join(MC_CLIENTS_DIR, f".{client_name}_old", item))
    shutil.rmtree(os.path.join(MC_CLIENTS_DIR, f".{client_name}"))
    os.rename(os.path.join(MC_CLIENTS_DIR, f".{client_name}_old"),
              os.path.join(MC_CLIENTS_DIR, f".{client_name}"))


def launch(nickname, uuid, access_token, version):
    version = version
    nickname = nickname if nickname else ""

    current_minecraft_version_dir = os.listdir(os.path.join(MC_CLIENTS_DIR, f".{version}", 'versions'))[0]
    current_minecraft_version_jar = ""

    for item in os.listdir(os.path.join(MC_CLIENTS_DIR, f".{version}", 'versions', current_minecraft_version_dir)):
        if ".jar" in item:
            current_minecraft_version_jar = item
            break

    mc_dir = os.path.join(MC_CLIENTS_DIR, f".{version}")

    natives_dir = os.path.join(mc_dir, 'versions', current_minecraft_version_dir, 'natives')
    client_json = json.loads(
        Path(
            os.path.join(mc_dir, "versions", current_minecraft_version_dir,
                         current_minecraft_version_jar.replace('.jar', '.json'))
        ).read_text())
    class_path = get_classpath(client_json, mc_dir)
    asset_index = client_json['assetIndex']['id'] if client_json.get('assetIndex') else None
    main_class = client_json['mainClass'] if client_json.get('mainClass') else 'net.minecraft.client.main.Main'

    minecraft_arguments = [
        'java',
        '-Xms6144m',
        '-Xmx6144m',
        '-Djavax.net.debug=all',
        '-Djavax.net.ssl.trustStore=cacerts',
        '-Djavax.net.ssl.trustStorePassword=changeit',
        f'-javaagent:{mc_dir}/libraries/com/mojang/authlib/authlib-injector-1.2.1.jar={AUTH_SERVER_HOST}',
        f'-Dauthlibinjector.debug',
        f'-Djava.library.path={natives_dir}',
        '-Dminecraft.launcher.brand=custom-launcher',
        '-Dminecraft.launcher.version=2.1',
        '-cp',
        class_path,
        main_class,
    ]

    items_values = {
        "username": nickname,
        "version": version,
        "gameDir": mc_dir,
        "assetsDir": os.path.join(mc_dir, 'assets'),
        "assetIndex": asset_index,
        "uuid": uuid,
        "userType": "mojang",
        "versionType": "release",
        "accessToken": access_token
    }

    args = client_json["minecraftArguments"].split("--")
    args.pop(0)
    for item in args:
        item = item.strip()
        items = item.strip().split(" ")
        item = items[0]
        cur_item_value = items[1]
        minecraft_arguments.append(f"--{item}")
        if "${" not in cur_item_value:
            minecraft_arguments.append(cur_item_value)
            continue
        if items_values.get(item):
            minecraft_arguments.append(f"{items_values[item]}")
    print(minecraft_arguments)
    subprocess.call(minecraft_arguments)


def login(session: requests.Session, username, password):
    request_data = {
        "nickname": username,
        "password": password
    }
    return session.post(f"{WEB_SERVER_HOST}/site/login/", json=request_data).json()



