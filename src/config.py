import os

LAUNCHER_TITLE = "Madness-project Launcher"

WEB_SERVER_HOST = "https://launcher.madness-project.ru/mineapi"
AUTH_SERVER_HOST = "https://launcher.madness-project.ru/mineapi"
DOWNLOAD_HOST = "https://launcher.madness-project.ru"

LAUNCHER_CLIENT_VERSIONS_DIR_NAME = "MP_LAUNCHER"

MC_CLIENTS_DIR = os.path.join(os.getenv('APPDATA'), LAUNCHER_CLIENT_VERSIONS_DIR_NAME)

clients = "/clients"
