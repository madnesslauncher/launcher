import json
import os
import shutil
import tkinter as tk
from tkinter.ttk import Combobox

import requests
from config import LAUNCHER_TITLE, MC_CLIENTS_DIR, WEB_SERVER_HOST

from utils import launch, get_available_versions, download_client, update_client, login


class MainWindow(tk.Tk):
    def __init__(self, original_path, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.session = requests.Session()
        self.init_user_data()
        self.original_path = original_path

        if not os.path.exists("cacerts"):
            shutil.copyfile(os.path.join(self.original_path, "cacerts"), os.path.join(os.getcwd(), 'cacerts'))

        user_data = self.get_user_data()["user_info"]

        self.nickname = user_data["nickname"]
        self.password = user_data["password"]
        self.uuid = ""
        self.access_token = ""

        self.title(LAUNCHER_TITLE)
        self.geometry("800x600")

        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (Main, Authorization):
            page_name = F.__name__
            frame = F(parent=container, controller=self)
            self.frames[page_name] = frame
            frame.grid(row=0, column=0, sticky="nsew")

        login_check = login(self.session, self.nickname, self.password)
        if login_check.get('message') != "Ok":
            self.show_frame("Authorization")
        else:
            self.update_credentials(login_check["profile"]["name"], self.password,
                                    login_check["profile"]["id"], login_check["token"])
            self.show_frame("Main")
            self.frames["Main"].on_open()

    def show_frame(self, page_name):
        frame = self.frames[page_name]
        frame.tkraise()

    def init_user_data(self):
        if not os.path.exists("user_data.json"):
            user_data_json = {
                "user_info": {
                    "nickname": "",
                    "password": ""
                }
            }

            with open("user_data.json", 'w') as data_file:
                json.dump(user_data_json, data_file)

    def save_credentials(self):
        new_data = self.get_user_data()
        new_data["user_info"]["nickname"] = self.nickname
        new_data["user_info"]["password"] = self.password
        with open("user_data.json", 'w') as data_file:
            json.dump(new_data, data_file)

    def get_user_data(self):
        with open("user_data.json") as json_file:
            user_data_json = json.load(json_file)
        return user_data_json

    def update_credentials(self, nickname, password, uuid, token):
        self.nickname = nickname
        self.uuid = uuid
        self.password = password
        self.access_token = token
        self.session.headers = {"Authorization": f"Token {token}"}
        self.save_credentials()


class Authorization(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.controller = controller
        self.nickname_input = tk.Entry(self, width=15)
        self.nickname_input.grid(column=0, row=0)

        self.password_input = tk.Entry(self, width=15)
        self.password_input.grid(column=0, row=1)

        self.error_label = tk.Label(self)
        self.error_label.grid(column=0, row=2)

        self.login_btn = tk.Button(self, text="Вход", command=self.login_action)
        self.login_btn.grid(column=0, row=3)

    def login_action(self):
        resp = login(self.controller.session, self.nickname_input.get(), self.password_input.get())
        if resp["message"] == "Error":
            self.error_label["text"] = resp["data"]["message"]
            return
        self.controller.update_credentials(resp["profile"]["name"], self.password_input.get(),
                                           resp["profile"]["id"], resp["token"])
        self.controller.show_frame("Main")
        self.controller.frames["Main"].on_open()


class Main(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        # TODO перенести версию архива в файлы клиента
        self.controller = controller

        self.installed_versions = Combobox(self, state="readonly")
        self.installed_versions['values'] = []
        self.installed_versions.grid(column=0, row=1)
        self.installed_versions.bind('<<ComboboxSelected>>', self.on_installed_version_field_change)
        self.btn = tk.Button(self, text="Запуск", command=lambda: launch(self.controller.nickname,
                                                                         self.controller.uuid,
                                                                         self.controller.access_token,
                                                                         self.installed_versions.get()))
        self.btn.grid(column=0, row=2)
        self.downloaded = tk.Label(self)
        # TODO Добавить возможность менять объем выделяемой памяти

    def on_open(self):
        self.installed_versions['values'] = get_available_versions(self.controller.session)
        self.installed_versions.current(0)
        self.on_installed_version_field_change(None)

    def on_installed_version_field_change(self, event):
        client_dir_path = os.path.join(MC_CLIENTS_DIR, f".{self.installed_versions.get()}")
        if not os.path.isdir(client_dir_path):
            need_to_update = None
        else:
            ver_file_path = os.path.join(client_dir_path, ".ver")
            if not os.path.isfile(ver_file_path):
                with open(ver_file_path, "w") as f:
                    f.write("unknown")
            with open(ver_file_path, "r") as f:
                version_hash = f.read()
            need_to_update = self.validate_version(version_hash)

        if need_to_update is None:
            self.btn["text"] = "Установить"
            self.btn["command"] = self.download_client_action
            self.btn.update()
            return
        if need_to_update:
            self.btn["text"] = "Обновить"
            self.btn["command"] = self.update_client
            self.btn.update()

    def download_client_action(self):
        self.btn["text"] = "Загрузка"
        self.btn["state"] = "disabled"
        downloaded_label = tk.Label(self)
        downloaded_label["text"] = "Загрузка:"
        downloaded_label.grid(column=0, row=5)
        download_client(downloaded_label, self.controller.session, self.installed_versions.get())
        self.btn["text"] = "Запуск"
        self.btn["state"] = "enabled"
        downloaded_label.destroy()
        self.btn["command"] = lambda: launch(self.controller.nickname,
                                             self.controller.uuid,
                                             self.controller.access_token,
                                             self.installed_versions.get())
        self.btn.update()

    def update_client(self):
        self.btn["text"] = "Загрузка"
        self.btn["state"] = "disabled"
        downloaded_label = tk.Label(self)
        downloaded_label["text"] = "Загрузка:"
        downloaded_label.grid(column=0, row=5)
        update_client(downloaded_label, self.controller.session, self.installed_versions.get())
        self.btn["text"] = "Запуск"
        self.btn["state"] = "enabled"
        downloaded_label.destroy()
        self.btn["command"] = lambda: launch(self.controller.nickname,
                                             self.controller.uuid,
                                             self.controller.access_token,
                                             self.installed_versions.get())
        self.btn.update()

    def validate_version(self, lastest_hash):
        json_data = {
            "client_name": self.installed_versions.get(),
            "users_version_hash": lastest_hash
        }
        response = self.controller.session.post(f"{WEB_SERVER_HOST}/launcher/compare-client-versions/", json=json_data)
        return response.json()["data"]["need_to_update"]
