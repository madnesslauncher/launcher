import config
from ui.main_window import MainWindow
import os


def main():
    original_path = os.getcwd()
    os.chdir(config.MC_CLIENTS_DIR)
    window = MainWindow(original_path=original_path)
    window.mainloop()


if __name__ == '__main__':
    main()
